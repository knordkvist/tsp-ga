Some general info and comments...
The source was compiled with Visual Studio 2010.
A new population is created using elitism (the 2 best chromosones are simply copied over),
multi point crossover and mutation.
The coordinate ranges, number of chromosones and number of cities are set as static constants in TSP.h,
crossover and mutation rates are sent as arguments to the constructor.
A new population is created until we haven't found a better solution for 10,000 generations.
This version will not stomp all over the heap, yay!